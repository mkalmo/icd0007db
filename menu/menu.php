<?php

require_once '../connection.php';
require_once 'MenuItem.php';

function getMenu(): array {

    $conn = getConnection();

    $stmt = $conn->prepare('SELECT id, parent_id, name 
                            FROM menu_item ORDER BY id');

    $stmt->execute();

    foreach ($stmt as $row) {
        $name = $row['name'];

        var_dump($name);
    }

    return [];
}












function printMenu($items, $level = 0) : void {
    $padding = str_repeat(' ', $level * 3);
    foreach ($items as $item) {
        printf("%s%s\n", $padding, $item->name);
        if ($item->subItems) {
            printMenu($item->subItems, $level + 1);
        }
    }
}
